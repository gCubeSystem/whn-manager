package org.gcube.resourcemanagement.whnmanager;

import java.util.HashSet;
import java.util.Set;

import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Application;

@Path("/")
public class WHNManager extends Application{


	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(ContextManager.class);
		return classes;
	}

}
