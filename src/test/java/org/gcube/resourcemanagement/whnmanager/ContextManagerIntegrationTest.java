package org.gcube.resourcemanagement.whnmanager;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import jakarta.servlet.ServletContext;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.gcube.common.events.Hub;
import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.context.Properties;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.lifecycle.application.ApplicationLifecycle;
import org.gcube.smartgears.persistence.PersistenceWriter;
import org.gcube.smartgears.security.secrets.SecretFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.BeforeClass;
import org.junit.Test;

public class ContextManagerIntegrationTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(ContextManager.class);
    }
    
    @BeforeClass
    public static void init() {
    	ContextProvider.set(new ApplicationContext() {
			
			@Override
			public Properties properties() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public PersistenceWriter persistence() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String name() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ApplicationLifecycle lifecycle() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String id() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Hub events() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ContainerContext container() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ApplicationConfiguration configuration() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public AuthorizationProvider authorizationProvider() {
				return new AuthorizationProvider() {

					@Override
					public Set<String> getContexts() {
						return Collections.singleton("/test/context");
					}

					@Override
					public Secret getSecretForContext(String context) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public Credentials getCredentials() {
						// TODO Auto-generated method stub
						return null;
					}
				};
			}
			
			@Override
			public ServletContext application() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path appSpecificConfigurationFolder() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<SecretFactory<? extends Secret>> allowedSecretFactories() {
				// TODO Auto-generated method stub
				return null;
			}
		});
    }
    
    @Test
    public void gettingContexts() {
        Response response = target("/contexts").request()
            .get();

        assertEquals("Http Response should be 200: ", Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

        String[] content = response.readEntity(String[].class);
        assertArrayEquals(content, new String[]{"/test/context"});
    }
}
