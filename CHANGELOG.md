This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for WhnManager

## [v3.0.0]

- Porting to smartgears 4

